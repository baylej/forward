#include <iostream>

#include <allegro5/allegro.h>
#include <allegro5/allegro_image.h>
#include <allegro5/allegro_primitives.h>
#include <allegro5/allegro_audio.h>
#include <allegro5/allegro_acodec.h>
#include <allegro5/allegro_font.h>

#include "main.hpp"
#include "failure.hpp"

#include "3D/debug.hpp"
#include "3D/renderer.hpp"

using std::cerr;
using std::endl;

// Initialize Allegro and its addons
Main::Main(int screen_w, int screen_h) {
	this->screen_h = screen_h;
	this->screen_w = screen_w;

	refresh_tick = 30;
	animation_tick = 60;

	if (!al_init()) throw Failure("failed to initialize allegro!");

	al_set_app_name(APP_NAME);
	al_set_org_name(APP_NAME);

	// Creates a modern OpenGL context
	al_set_new_display_flags(ALLEGRO_OPENGL);
	//al_set_new_display_flags(ALLEGRO_OPENGL | ALLEGRO_OPENGL_3_0); // compatibility context
	//al_set_new_display_flags(ALLEGRO_OPENGL | ALLEGRO_OPENGL_FORWARD_COMPATIBLE); // core context (modern opengl)
	//al_set_new_display_flags(ALLEGRO_OPENGL | ALLEGRO_OPENGL_3_0 | ALLEGRO_PROGRAMMABLE_PIPELINE); // using Allegro's Shader Manager

	// Depth buffer (aka ZBuffer)
	al_set_new_display_option(ALLEGRO_DEPTH_SIZE, 24, ALLEGRO_SUGGEST);

	// Multisampling (antialiasing)
	al_set_new_display_option(ALLEGRO_SAMPLE_BUFFERS, 1, ALLEGRO_SUGGEST);
	al_set_new_display_option(ALLEGRO_SAMPLES, 4, ALLEGRO_SUGGEST); // 4x

	// Creates the Display
	if (display = al_create_display(screen_w, screen_h), !display) throw Failure("failed to create display!");

	// loads every OpenGL functions
	if (!ogl_LoadFunctions()) throw Failure("OpenGL core profile 4.3 load failed");

	// Clears the GL error buffer
	GLenum err;
	while (err = glGetError(), err != GL_NO_ERROR) {
		cerr << "Gl Error: " << err << endl;
	}

	// Print OpenGL version, Samples count and Depth size
	cerr << "OpenGl: " << glGetString(GL_VERSION) << endl;
	cerr << "Samples: " << al_get_display_option(display, ALLEGRO_SAMPLES) << endl;
	cerr << "DepthSize: " << al_get_display_option(display, ALLEGRO_DEPTH_SIZE) << endl;

	// Enable Depth Test (tells OpenGL which sample must be drawn in the framebuffer) (3D)
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glClearDepth(1.0);

	// Background color, used by glClear(GL_COLOR_BUFFER_BIT)
	glClearColor(55/255.f, 55/255.f, 55/255.f, 1.f);

	// Enable culling (render visible triangles only) (3D)
	glEnable(GL_CULL_FACE);
	//glCullFace(GL_BACK);
	glFrontFace(GL_CCW); // CounterClockWise triangles vertices

	if (!al_install_keyboard()) throw Failure("failed to initialize the keyboard!");
	if (!al_install_mouse()) throw Failure("failed to initialize the mouse!");
	//if (!al_install_joystick()) throw Failure("failed to initialize the joystick!");

	//if (!(refreshEQ = al_create_event_queue())) throw Failure("failed to create the frame event queue!");
	if (!(animationEQ = al_create_event_queue())) throw Failure("failed to create the animation event queue!");
	if (!(inputEQ = al_create_event_queue())) throw Failure("failed to create the input event queue!");

	//if (!(refreshTimer = al_create_timer(1.0/refresh_tick))) throw Failure("failed to initialise the frame timer!");
	if (!(animationTimer = al_create_timer(1.0/animation_tick))) throw Failure("failed to initialise the animation timer!");

	if (!al_init_image_addon()) throw Failure("failed to initialise image support!");

	//if (!al_install_audio()) throw Failure("failed to initialise audio support!");
	//if (!al_init_acodec_addon()) throw Failure("failed to initialise audio codecs support!");

	//if (!al_init_primitives_addon()) throw Failure("failed to initialise primitives!");

	//al_init_font_addon();
	//if (!(console_font = al_create_builtin_font())) throw Failure("failed to initialise console font!");

	// ALLEGRO_EVENT_DISPLAY_*
	al_register_event_source(inputEQ, al_get_display_event_source(display));
	// ALLEGRO_EVENT_JOYSTICK_*
	//al_register_event_source(inputEQ, al_get_joystick_event_source());
	// ALLEGRO_EVENT_KEY_*
	al_register_event_source(inputEQ, al_get_keyboard_event_source());

	// ALLEGRO_EVENT_TIMER frame update
	//al_register_event_source(frameEQ, al_get_timer_event_source(frameTimer));
	// ALLEGRO_EVENT_TIMER animation update
	al_register_event_source(animationEQ, al_get_timer_event_source(animationTimer));
}

// Deinitialize Allegro and its addons
Main::~Main() {
	//al_destroy_font(console_font);

	//al_shutdown_primitives_addon();

	//al_uninstall_audio();

	al_shutdown_image_addon();

	al_destroy_timer(animationTimer);
	//al_destroy_timer(frameTimer);

	al_destroy_event_queue(inputEQ);
	al_destroy_event_queue(animationEQ);
	//al_destroy_event_queue(refreshEQ);

	//al_uninstall_joystick();
	al_uninstall_mouse();
	al_uninstall_keyboard();

	al_destroy_display(display);
}

/** OpenGL 3.3+ modern **/
int main(void) {
	try {
		Main m(800, 600);
		OpenGLLogger logger;
		logger.registerOpenGLLogger();
		glEnable(GL_DEBUG_OUTPUT);
		glm::mat4 model;
		Renderer r(800, 600, 1., 100., 45., 20, 20, 4);
		Cube c;
		PointLight pl;

		// Sets up the scene
		r.camera.setPosition(15., 8., 15.);
		r.camera.setOrientation(-ALLEGRO_PI/8., ALLEGRO_PI/4., 0.);

		for (int i=0; i<=8; i++) {
			for (int j=0; j<=8; j++) {
				r.geometry.push_back(c);
				r.geom_model.push_back(glm::translate(m4id, glm::vec3((i-4)*2.5, 0, (j-4)*2.5)));
			}
		}

		r.geometry.push_back(c);
		model = glm::scale(glm::translate(m4id, glm::vec3(-2, -2, -2)), glm::vec3(25., 1., 25.));
		r.geom_model.push_back(model);

		// Adds Lights
		glm::vec3 light_pos[4] = {
			glm::vec3(-11, .25, -11),
			glm::vec3(-11, .25,  11),
			glm::vec3( 11, .25, -11),
			glm::vec3( 11, .25,  11)
		};
		pl.intensity = 1.f;
		pl.color[0] = pl.color[1] = pl.color[2] = .8;
		for (int i=0; i<4; i++) {
			pl.position = glm::vec4(light_pos[i], 0);
			r.light.addLight(pl);
		}
		r.light.loadLights();

		// Main event loop
		ALLEGRO_EVENT ev;
//		int step = 0;
		al_start_timer(m.animationTimer);
		bool loop = true;
		while (al_wait_for_event(m.animationEQ, &ev), loop) {

			// Animation
//			r.geom_model[0] = glm::scale(glm::rotate(glm::rotate(m4id, 0.005f*step, Yu), 0.002f*step, Xu), glm::vec3(3., 3., 3.));
//			step++;

			r.render();
			al_flip_display();

			// Input events
			while (al_get_next_event(m.inputEQ, &ev)) {
				if (ev.type == ALLEGRO_EVENT_DISPLAY_CLOSE) {
					loop = false;
				}
				else if (ev.type == ALLEGRO_EVENT_KEY_DOWN) {
					glm::vec3 forward = r.camera.getForwardVector();
					glm::vec3 right   = r.camera.getRightVector();
					glm::vec3 up      = r.camera.getUpVector();

					switch (ev.keyboard.keycode) {
						case ALLEGRO_KEY_ESCAPE: // Exits
							loop = false;
							break;
						case ALLEGRO_KEY_PAD_5: {
							r.camera.incPosition( forward);
							break;
						}
						case ALLEGRO_KEY_PAD_0: {
							r.camera.incPosition(-forward);
							break;
						}
						case ALLEGRO_KEY_PAD_4: {
							r.camera.incPosition(-right);
							break;
						}
						case ALLEGRO_KEY_PAD_6: {
							r.camera.incPosition( right);
							break;
						}
						case ALLEGRO_KEY_PAD_2: {
							r.camera.incPosition(-up);
							break;
						}
						case ALLEGRO_KEY_PAD_8: {
							r.camera.incPosition( up);
							break;
						}
						case ALLEGRO_KEY_F2: { // Produces a bitmap of light density par tile
							ALLEGRO_BITMAP *bmp = r.tilesDensity();
							al_save_bitmap("debug.tga", bmp);
							break; }
						case ALLEGRO_KEY_F3: { // Produces a file containing the depth texture as a float array
							float *depth = r.depthComp();
							ALLEGRO_FILE *out = al_fopen("depth.float_array", "wb");
							al_fwrite(out, depth, 800*600*sizeof(float));
							al_fflush(out);
							al_fclose(out);
							break; }
						default:
							std::cout << "Key " << al_keycode_to_name(ev.keyboard.keycode) << " not mapped" << std::endl;
					}
				}
			}
		}
	}
	catch (Failure f) {
		cerr << f.what() << endl;
	}
	return 0;
}
