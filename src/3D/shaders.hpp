/**
	Shader & Shader Manager.
**/
#pragma once
#ifndef SHADERS_H
#define SHADERS_H

#include "glw.hpp"

class Shaders {
	bool defLinked;
public:
	GLuint depthPrepassProgId;
	GLuint lightCullProgId;
	GLuint shadingProgId;
	GLuint postProcProgId;

	static const char *SHADING_VERT_SHADER_PRGM;
	static const char *SHADING_FRAG_SHADER_PRGM;

	static const char *LIGHTCULL_COMP_SHADER_PRGM;

	static const char *DEPTH_VERT_SHADER_PRGM;
	static const char *DEPTH_FRAG_SHADER_PRGM;

	static const char *POSTPROC_VERT_SHADER_PRGM;
	static const char *POSTPROC_FRAG_SHADER_PRGM;

	Shaders(): defLinked(false) {}

	void loadDefaultShaders();
	void unloadDefaultShaders();

	static GLuint loadShader(GLuint program, GLenum shaderType, const GLchar *source);
	static void linkProgram(GLuint program);
};

#endif // SHADERS_H
