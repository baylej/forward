#include <cstdlib>

#include "lights.hpp"

/// `4*sizeof(GLint)` bytes before light array's addresses, an int contains the size of the array
/// The complete array (starting at the address of the int) is loaded in the GPU RAM

// AutoReAlloclength
#define ARA_LEN 10

/// Array auto realloc
/// Returns the new array_len
int auto_incr(void **array, size_t el_size, int array_capacity, int data_size) {
	if (data_size == array_capacity) {
		array_capacity += + ARA_LEN;
		*array = realloc(((GLint*)*array)-4, array_capacity * el_size + 4*sizeof(GLint));
		*array = (GLint*) *array + 4; // Array size at the pointer
	}
	return array_capacity;
}

Lights::Lights(): Lights(0, 0, 0) {}

Lights::Lights(unsigned int dl_cap, unsigned int pl_cap, unsigned int sl_cap) {
	GLint *tmp = (GLint*) malloc(dl_cap * sizeof(DirectionalLight) + 4*sizeof(GLint));
	this->directionalLights = (DirectionalLight*) (tmp+4);

	tmp = (GLint*) malloc(pl_cap * sizeof(PointLight) + 4*sizeof(GLint));
	this->pointLights = (PointLight*) (tmp+4);

	tmp = (GLint*) malloc(sl_cap * sizeof(SpotLight) + 4*sizeof(GLint));
	this->spotLights = (SpotLight*) (tmp+4);

	this->dl_cap = dl_cap;
	this->pl_cap = pl_cap;
	this->sl_cap = sl_cap;

	this->dl_size = this->pl_size = this->sl_size = 0;

	this->ssboDireL = this->ssboPoinL = this->ssboSpotL = 0;
}

void Lights::addLight(PointLight &l) {
	this->pl_cap = auto_incr((void**) &(this->pointLights), sizeof(PointLight), this->pl_cap, this->pl_size);
	this->pointLights[this->pl_size] = l;
	this->pl_size++;
}

void Lights::addLight(DirectionalLight &l) {
	this->dl_cap = auto_incr((void**) &(this->directionalLights), sizeof(DirectionalLight), this->dl_cap, this->dl_size);
	this->directionalLights[this->dl_size] = l;
	this->dl_size++;
}

void Lights::addLight(SpotLight &l) {
	this->sl_cap = auto_incr((void**) &(this->spotLights), sizeof(SpotLight), this->sl_cap, this->sl_size);
	this->spotLights[this->sl_size] = l;
	this->sl_size++;
}

void Lights::loadLights() {
	int size;
	GLint *adr;

	// Generates GL names if not generated yet
	if (!this->ssboPoinL) {
		GLuint ssbo[3];
		glGenBuffers(3, ssbo);
		this->ssboPoinL = ssbo[0];
		this->ssboDireL = ssbo[1];
		this->ssboSpotL = ssbo[2];
	}

	// Binds buffers and loads light datas
	// Point Lights
	size = this->pl_size * sizeof(PointLight) + 4*sizeof(GLint);
	adr = ((GLint*)this->pointLights)-4;
	*adr = this->pl_size;
	glBindBuffer(GL_SHADER_STORAGE_BUFFER, this->ssboPoinL);
	glBufferData(GL_SHADER_STORAGE_BUFFER, size, adr, GL_STATIC_DRAW);

	// Directional lights
	size = this->dl_size * sizeof(DirectionalLight) + 4*sizeof(GLint);
	adr = ((GLint*)this->directionalLights)-4;
	*adr = this->dl_size;
	glBindBuffer(GL_SHADER_STORAGE_BUFFER, this->ssboDireL);
	glBufferData(GL_SHADER_STORAGE_BUFFER, size, adr, GL_STATIC_DRAW);

	// Spot lights
	size = this->sl_size * sizeof(SpotLight) + 4*sizeof(GLint);
	adr = ((GLint*)this->spotLights)-4;
	*adr = this->sl_size;
	glBindBuffer(GL_SHADER_STORAGE_BUFFER, this->ssboSpotL);
	glBufferData(GL_SHADER_STORAGE_BUFFER, size, adr, GL_STATIC_DRAW);
}

void Lights::bindPointLights(GLuint binding) {
	glBindBufferBase(GL_SHADER_STORAGE_BUFFER, binding, ssboPoinL);
}

void Lights::bindDirectionalLights(GLuint binding) {
	glBindBufferBase(GL_SHADER_STORAGE_BUFFER, binding, ssboDireL);
}

void Lights::bindSpotLights(GLuint binding) {
	glBindBufferBase(GL_SHADER_STORAGE_BUFFER, binding, ssboSpotL);
}
