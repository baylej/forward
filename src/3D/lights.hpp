/**
	Lights (Point, directional, Spot)
 **/
#pragma once
#ifndef LIGHTS_HPP
#define LIGHTS_HPP

#include "glw.hpp"

/*
	Warning from opengl.org about ssbo layouts:
		Implementations sometimes get the std140(std430) layout wrong for vec3 components.
		You are advised to avoid using vec3 at all.

	Note about std430:
		The array stride (the bytes between array elements) is always rounded up to the size of a vec4 (ie: 16-bytes).

	In the following structures, I use vec4, the last component (`w`) must be ignored.
*/


/// PointLight
class PointLight {
public:
	glm::vec4 position;
	glm::vec4 color;
	GLfloat   intensity;
	GLfloat   padding[3];
};

/// DirectionalLight
class DirectionalLight {
public:
	glm::vec4 direction;
	glm::vec4 color;
	GLfloat   intensity;
	GLfloat   padding[3];
};

/// SpotLight
class SpotLight {
public:
	glm::vec4 position;
	glm::vec4 color;
	glm::vec4 direction;
	GLfloat   intensity;
	GLfloat   angle;
	GLfloat   penumbraAngle;
	GLfloat   padding;
};

/// Holds the lights arrays and GL names
class Lights {
	/// Array capacities
	unsigned int dl_cap;
	unsigned int pl_cap;
	unsigned int sl_cap;
public:
	/// Data size
	unsigned int dl_size;
	unsigned int pl_size;
	unsigned int sl_size;

	/// Arrays
	DirectionalLight *directionalLights;
	PointLight       *pointLights;
	SpotLight        *spotLights;

	/// SSBOs (OpenGL GPURAM ReadWrite array)
	GLuint ssboPoinL;
	GLuint ssboDireL;
	GLuint ssboSpotL;

	/// Constructor
	Lights();
	/// Constuctor with initial array sizes
	Lights(unsigned int dl_size, unsigned int pl_size, unsigned int sl_size);

	/// Adders
	void addLight(DirectionalLight &dl);
	void addLight(PointLight &pl);
	void addLight(SpotLight &sl);

	/// Loads each kind of light in its SSBO
	void loadLights();

	/// Binds a Light SSBO at the given binding in the active shader
	void bindPointLights(GLuint binding);
	void bindDirectionalLights(GLuint binding);
	void bindSpotLights(GLuint binding);
};

#endif // LIGHTS_HPP
