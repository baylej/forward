#include "renderer.hpp"

#include <cmath>

/// Prepare the OpenGL context for rendering :
/// Loads shaders, uniform locations, FBOs and their textures
Renderer::Renderer(int w, int h, float near, float far, float fov, int tile_w, int tile_h, int max_lights_per_tiles)
{
	this->near = near;
	this->far = far;
	this->fov = fov;
	this->tile_h = tile_h;
	this->tile_w = tile_w;
	this->max_lights_per_tiles = max_lights_per_tiles;

	// Depth pre-pass texture & fbo
	glGenTextures(1, &(depth_tex));
	glBindTexture(GL_TEXTURE_2D, depth_tex);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT24, w, h, 0, GL_DEPTH_COMPONENT, GL_FLOAT, 0);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S,     GL_CLAMP_TO_EDGE);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T,     GL_CLAMP_TO_EDGE);GLERRCHK

	glGenFramebuffers(1, &fbo_depth);
	glBindFramebuffer(GL_FRAMEBUFFER, fbo_depth);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, depth_tex, 0);

	if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE) {
		std::cerr << "Error on building framebuffer\n";
		std::exit(1);
	}
	glBindFramebuffer(GL_FRAMEBUFFER, 0);

	// Shaders: uniforms & subroutines
	shaders.loadDefaultShaders();

	d_proj_mat  = glGetUniformLocation(shaders.depthPrepassProgId, "proj_m4");
	d_view_mat  = glGetUniformLocation(shaders.depthPrepassProgId, "view_m4");
	d_model_mat = glGetUniformLocation(shaders.depthPrepassProgId, "model_m4");

	lc_proj_mat            = glGetUniformLocation(shaders.lightCullProgId, "proj_m4");
	lc_view_mat            = glGetUniformLocation(shaders.lightCullProgId, "view_m4");
	lc_camera_pos_3d       = glGetUniformLocation(shaders.lightCullProgId, "camera_pos_3d");
	lc_screen_size         = glGetUniformLocation(shaders.lightCullProgId, "screen_size");
	lc_tile_size           = glGetUniformLocation(shaders.lightCullProgId, "tile_size");
	lc_max_lights_per_tile = glGetUniformLocation(shaders.lightCullProgId, "max_lights_per_tile");
	lc_depth               = glGetUniformLocation(shaders.lightCullProgId, "depth");

	s_proj_mat  = glGetUniformLocation(shaders.shadingProgId, "proj_m4");
	s_view_mat  = glGetUniformLocation(shaders.shadingProgId, "view_m4");
	s_model_mat = glGetUniformLocation(shaders.shadingProgId, "model_m4");

	pp_none            = glGetSubroutineIndex(shaders.postProcProgId, GL_FRAGMENT_SHADER, "none");
	pp_linearize_depth = glGetSubroutineIndex(shaders.postProcProgId, GL_FRAGMENT_SHADER, "linearizeDepth");
	pp_texture = glGetUniformLocation(shaders.postProcProgId, "tex");

	// Light index SSBO
	ssbo_light_indices = 0;
	resize(w, h);
	GLERRCHK
}

/// Undo what the constructor did (OpenGL context cleanup)
Renderer::~Renderer()
{
	glDeleteBuffers(1, &ssbo_light_indices);
	// Reverse order of constructor
	shaders.unloadDefaultShaders();
	glDeleteFramebuffers(1, &fbo_depth);
	glDeleteTextures(1, &depth_tex);
}

void Renderer::resize(int w, int h) {
	this->win_h = h;
	this->win_w = w;
	camera.frustum(w, h, near, far, fov);

	win_tile_w = std::ceil(win_w/(float)tile_w);
	win_tile_h = std::ceil(win_h/(float)tile_h);

	tile_count = win_tile_w * win_tile_h;

	if (ssbo_light_indices != 0) {
		glDeleteBuffers(1, &ssbo_light_indices);
	}

	glGenBuffers(1, &ssbo_light_indices);
	glBindBuffer(GL_SHADER_STORAGE_BUFFER, ssbo_light_indices);
	glBufferData(GL_SHADER_STORAGE_BUFFER, tile_count * max_lights_per_tiles * sizeof(GLint), NULL, GL_DYNAMIC_COPY);
}

void Renderer::render() {
	// Depth pre-pass
	prePass();

	// Light culling
	// The calling code must call lights.load... before calling render()
	lightCull();

	// Shading
//	shading();

	// Post processing
	postProc(depth_tex, pp_linearize_depth);
	GLERRCHK
}

void Renderer::prePass() {
	glUseProgram(shaders.depthPrepassProgId);

	camera.foenum();
	camera.loadProjectionMatrix(d_proj_mat);
	camera.loadViewMatrix(d_view_mat);

	glBindFramebuffer(GL_FRAMEBUFFER, fbo_depth);
	glClear(GL_DEPTH_BUFFER_BIT); // Clears the depth buffer of `fbo_depth`

	drawGeometry(d_model_mat);

	glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

void Renderer::lightCull() {
	glUseProgram(shaders.lightCullProgId);

	camera.loadProjectionMatrix(lc_proj_mat);
	camera.loadViewMatrix(lc_view_mat);

	glUniform3fv(lc_camera_pos_3d, 1, glm::value_ptr(camera.getPosition()));
	glUniform2f(lc_screen_size, win_w, win_h);
	glUniform2f(lc_tile_size, tile_w, tile_h);
	glUniform1i(lc_max_lights_per_tile, max_lights_per_tiles);

	// Actives texture unit#0, loads the depth texture in unit#0, and use unit#0
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, depth_tex);
	glUniform1i(lc_depth, 0);

	// Binds SSBOs
	this->light.bindPointLights(0);
	this->light.bindSpotLights(1);
	// DirectionalLights are global: no need to cull this kind of light
	glBindBufferRange(GL_SHADER_STORAGE_BUFFER, 2, ssbo_light_indices, 0, tile_count * max_lights_per_tiles * sizeof(GLint));

	glDispatchCompute(win_tile_w, win_tile_h, 1);
}

void Renderer::shading() {
	glUseProgram(shaders.shadingProgId);

	camera.foenum();
	camera.loadProjectionMatrix(s_proj_mat);
	camera.loadViewMatrix(s_view_mat);

	glClear(GL_DEPTH_BUFFER_BIT | GL_COLOR_BUFFER_BIT);
	drawGeometry(s_model_mat);
}

void Renderer::postProc(GLuint tex_name, GLuint pp_func_name) {
	static Plane plane;
	glClear(GL_COLOR_BUFFER_BIT);
	glUseProgram(shaders.postProcProgId);
	glUniformSubroutinesuiv(GL_FRAGMENT_SHADER, 1, &pp_func_name);

	// Use Texture unit#0, binds the depth texture to unit#0, sets pp_texture to unit#0
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, tex_name);
	glUniform1i(pp_texture, 0);

	glDisable(GL_DEPTH_TEST);
	plane.draw();
	glEnable(GL_DEPTH_TEST);
}

void Renderer::drawGeometry(GLuint model_m4_target) {
	if (geometry.size() == geom_model.size()) {
		for (unsigned int i=0; i<geometry.size(); i++) {
			glUniformMatrix4fv(model_m4_target, 1, GL_FALSE, glm::value_ptr(geom_model[i]));
			geometry[i].draw();
		}
	}
}

ALLEGRO_COLOR density_lut[8] = {
	{0., 0., 1., 1.},
	{0., .4, 1., 1.},
	{0., .7, 1., 1.},
	{0., 1., 1., 1.},
	{0., 1., 0., 1.},
	{1., 1., 0., 1.},
	{1., .5, 0., 1.},
	{1., 0., 0., 1.},
};

ALLEGRO_BITMAP* Renderer::tilesDensity() {
	ALLEGRO_BITMAP *bmp, *prev;
	GLvoid *ptr;
	GLint *light_ind;
	int x, y, i, tile_ind_offset, light, count, format, flags;

	glBindBuffer(GL_SHADER_STORAGE_BUFFER, ssbo_light_indices);
	ptr = glMapBuffer(GL_SHADER_STORAGE_BUFFER, GL_READ_ONLY);
	light_ind = (GLint*) ptr;

	flags = al_get_new_bitmap_flags();
	al_set_new_bitmap_flags(ALLEGRO_MEMORY_BITMAP);
	format = al_get_new_bitmap_format();
	al_set_new_bitmap_format(ALLEGRO_PIXEL_FORMAT_RGB_888);
	bmp = al_create_bitmap(win_tile_w, win_tile_h);
	al_set_new_bitmap_format(format);
	al_set_new_bitmap_flags(flags);
	prev = al_get_target_bitmap();
	al_set_target_bitmap(bmp);
	al_lock_bitmap(bmp, ALLEGRO_PIXEL_FORMAT_RGB_888, ALLEGRO_LOCK_WRITEONLY);

	for (x=0; x<win_tile_w; x++) {
		for (y=0; y<win_tile_h; y++) {
			count = 0;
			tile_ind_offset = (x * win_tile_h + y) * max_lights_per_tiles;
			for (i=0; i<max_lights_per_tiles; i++) {
				light = light_ind[tile_ind_offset + i];
				if (light == -1) break;
				count++;
			}
			if (count >= 1)
				std::cout << "tile " << x << ',' << y << " = " << count << " lights" << std::endl;
			count = (count / (float)max_lights_per_tiles) * 7;
			al_put_pixel(x, win_tile_h-1-y, density_lut[count]);
		}
	}

	al_unlock_bitmap(bmp);
	al_set_target_bitmap(prev);
	glUnmapBuffer(GL_SHADER_STORAGE_BUFFER);
	return bmp;
}

float* Renderer::depthComp() {
	float *res = new float[win_h * win_w];
	glBindTexture(GL_TEXTURE_2D, depth_tex);
	glGetTexImage(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT, GL_FLOAT, res);
	return res;
}
