#include <iostream>

#include "shaders.hpp"
#include "../failure.hpp"

/// Forward+ shaders

/// Depth pre-pass shaders
const char* Shaders::DEPTH_VERT_SHADER_PRGM = 
	"#version 430 core\n"
	"layout(location=0) in vec3 vx_pos;" // Vertice

	"uniform mat4 proj_m4;"
	"uniform mat4 view_m4;"
	"uniform mat4 model_m4;"

	"void main() {"
		"gl_Position = proj_m4 * view_m4 * model_m4 * vec4(vx_pos, 1.0);"
	"}";

const char* Shaders::DEPTH_FRAG_SHADER_PRGM =
	"#version 430 core\n"
	"void main() {"
	"}";

// ----

/// Light culling pass
const char* Shaders::LIGHTCULL_COMP_SHADER_PRGM = 
	"#version 430 core\n"
	"layout (local_size_x = 1, local_size_y = 1, local_size_z = 1) in;"

	// To translate light's coordinates to screen coordinates
	"uniform mat4 proj_m4;"
	"uniform mat4 view_m4;"

	// Camera position in space coordinates, for light culling
	"uniform vec3 camera_pos_3d;"

	"uniform vec2 screen_size;"
	"uniform vec2   tile_size;"
	"uniform int max_lights_per_tile;"

	// Depth texture made by the depth pre-pass
	"uniform sampler2D depth;"

	// Defines lights structures
	"struct PointLight {"
		"vec4 position;"
		"vec4 color;"
		"float intensity;"
	"};"

	"struct SpotLight {"
		"vec4 position;"
		"vec4 color;"
		"vec4 direction;"
		"float intensity;"
		"float angle;"
		"float penumbra_angle;"
	"};"

	// Light SSBOs (input)
	"layout(std430, binding = 0) buffer _pointLights {"
		"uint count;"
		"PointLight lights[];"
	"} point_lights;"

	"layout(std430, binding = 1) buffer _spotLights {"
		"uint count;"
		"SpotLight lights[];"
	"} spot_lights;"

	// Light indices SSBO (output)
	"layout(std430, binding = 2) buffer _indices {"
		"int indices[];" // length = count(tiles) * max_lights_per_tile
	"} indices;"

	// Computes the boundaries of the current tile, vec4 = (bl, ur)
	// bl = vec2(x, y) = bottom left  corner coordinates
	// ur = vec2(x, y) = upper  right corner coordinates
	"vec4 getTileBounds() {"
		"vec2 bl = gl_WorkGroupID.xy;" // WorkGroupID === TileID
		"bl = bl * tile_size;"
		"vec2 ur = bl + tile_size;"
		"if (ur.x > screen_size.x) ur.x = screen_size.x;"
		"if (ur.y > screen_size.y) ur.y = screen_size.y;"
		"return vec4(bl, ur);"
	"}"

	// Computes the depth frustum (Min and Max depth values) of the current tile
	// Returns vec2(min, max)
	"vec2 getDepthFrustum(vec4 boundaries) {"
		"float i, j;"
		"float min = 1., max = 0., d;"
		"for (i=boundaries.x; i<boundaries.z; i++) {"
			"for (j=boundaries.y; j<boundaries.w; j++) {"
				"d = texture(depth, vec2(i/screen_size.x, j/screen_size.y)).r;"
				"if (d > max) max = d;"
				"if (d < min) min = d;"
			"}"
		"}"
		"return vec2(min, max);"
	"}"

	// Returns the frustum of the current tile. mat3x2 = (xf, yf, zf)
	// xf = vec2(x1, x2)  yf = vec2(y1, y2)  zf = vec2(z1, z2)  .1 <= .2
	"mat3x2 getTileFrustum() {"
		"vec4 tile_bounds = getTileBounds();"
		"vec2 tile_depth  = getDepthFrustum(tile_bounds);"
		"tile_bounds.x = tile_bounds.x / (screen_size.x / 2.) -1;" // [0:win_w) to [-1:1] coordinates
		"tile_bounds.y = tile_bounds.y / (screen_size.y / 2.) -1;"
		"tile_bounds.z = tile_bounds.z / (screen_size.x / 2.) -1;"
		"tile_bounds.w = tile_bounds.w / (screen_size.y / 2.) -1;"
		"return mat3x2(tile_bounds.xz, tile_bounds.yw, tile_depth);"
	"}"

	// GLSL 430 has a distance() function, but there is no built-in squareDistance function
	"float sqrDistance(vec4 a, vec4 b) {"
		"vec4 ab = b - a;"
		"return ab.x * ab.x + ab.y * ab.y + ab.z * ab.z;"
	"}"
	"float sqrDistance(vec3 a, vec3 b) { return sqrDistance(vec4(a, 1.), vec4(b, 1.)); }"

	// Returns the frustum of a pointlight's area of effect. mat3x2 = (xf, yf, zf)
	// xf = vec2(x1, x2)  yf = vec2(y1, y2)  zf = vec2(z1, z2)  .1 <= .2
	"mat3x2 pointLightFrustum(int light_ind) {"
		"vec3 pos = point_lights.lights[light_ind].position.xyz;"
		"float intensity = point_lights.lights[light_ind].intensity;"
		"float att = intensity * 10. / sqrDistance(camera_pos_3d, pos);" // FIXME if ==0
		"float aspect_ratio = screen_size.x/float(screen_size.y);"
		"vec4 eye = view_m4 * vec4(pos, 1.);" // eye coordinates
		"vec4 scrn = proj_m4 * vec4(0., 0., eye.z+intensity, 1.);"
		"vec4 scrf = proj_m4 * vec4(0., 0., eye.z-intensity, 1.);"
		"float zn = (scrn.z / scrn.w) * .5 + .5;" // Z bounds of this light's frustum
		"float zf = (scrf.z / scrf.w) * .5 + .5;"
		"vec4 clip = proj_m4 * eye;" // clip coordinate
		"pos = clip.xyz / clip.w;" // screen coordinates (normalized)
		"return mat3x2(pos.x - att, pos.x + att, pos.y - (att * aspect_ratio), pos.y + (att * aspect_ratio), zn, zf);"
	"}"

	"bool m_sign(float f) {"
		"return f >= 0.;"
	"}"

	// Returns true if the given light overlaps with the given frustum.
	"bool overlaps(int light_ind, mat3x2 frustum) {"
		"mat3x2 light_frustum = pointLightFrustum(light_ind);"
		"float lx, ly, lz, tx, ty, tz;" // x,y,z axis

		"lx = frustum[0].y - light_frustum[0].x;"
		"tx = light_frustum[0].y - frustum[0].x;"

		"ly = frustum[1].y - light_frustum[1].x;"
		"ty = light_frustum[1].y - frustum[1].x;"

		"lz = frustum[2].y - light_frustum[2].x;"
		"tz = light_frustum[2].y - frustum[2].x;"

		"return m_sign(lx) == m_sign(tx) && m_sign(ly) == m_sign(ty) && m_sign(lz) == m_sign(tz);"
	"}"

	// Adds the given light index to this tile's light list
	// Returns false if the light index list is full, true otherwise
	"int tile_ind_offset = 0;"
	"int current_index = 0;"
	"bool addLight(int light_ind) {"
		"if (current_index == max_lights_per_tile) return false;"
		"indices.indices[tile_ind_offset + current_index] = light_ind;"
		"current_index++;"
		"return true;"
	"}"

	// Marks the end of the `indices` SSBO with a '-1' (just like '\0' for a String)
	"void endOfArrayMark() {"
		"if (current_index != max_lights_per_tile) indices.indices[tile_ind_offset + current_index] = -1;"
	"}"

	"void main() {"
		// Compute this tile's offset in output SSBO `indices`
		"tile_ind_offset = int(gl_WorkGroupID.x * ceil(screen_size.y / tile_size.y) + gl_WorkGroupID.y) * max_lights_per_tile;"

		// Frustum of the tile
		"mat3x2 tile_frustum = getTileFrustum();"

		// For all the lights
		"for (int i=0; i<point_lights.count; i++) {"
			// If it overlaps with the current tile
			"if (overlaps(i, tile_frustum)) {"
				// Adds it to this tile's list of lights
				"if (!addLight(i)) break;"
			"}"
		"}"

		"endOfArrayMark();"
	"}";

// ----

/// Shading pass
const char* Shaders::SHADING_VERT_SHADER_PRGM = 
	"#version 430 core\n"
	"layout(location=0) in vec3 vx_pos;" // Vectice
	"layout(location=1) in vec3 vx_nrm;" // Normal
	"layout(location=2) in vec2 vx_tuv;" // Texture UV
	"layout(location=3) in vec3 vx_col;" // Color

	"uniform mat4 proj_m4;"
	"uniform mat4 view_m4;"
	"uniform mat4 model_m4;"

	"out vec3 color;"

	"void main() {"
		"gl_Position = proj_m4 * view_m4 * model_m4 * vec4(vx_pos, 1.0);"
		"color = vx_col;"
	"}";

const char* Shaders::SHADING_FRAG_SHADER_PRGM =
	"#version 430 core\n"
	"in vec3 color;"

	"layout(location = 0) out vec4 fragCol;"

	"void main() {"
		"fragCol = vec4(color, 1.0);"
	"}";

// ----

/// Post processing pass
const char* Shaders::POSTPROC_VERT_SHADER_PRGM =
	"#version 430 core\n"

	"layout(location=0) in vec2 vx_pos;"
	"layout(location=2) in vec2 vx_tuv;"
	"out vec2 tex_uv;"

	"void main() {"
		"gl_Position = vec4(vx_pos, 0., 1.);"
		"tex_uv = vx_tuv;"
	"}";

const char* Shaders::POSTPROC_FRAG_SHADER_PRGM =
	"#version 430 core\n"

	"uniform sampler2D tex;"
	"in vec2 tex_uv;"
	"layout(location = 0) out vec4 fragCol;"

	// OpenGL SubRoutines postProcess functions
	"subroutine void postProc();"
	"subroutine uniform postProc post_proc;"

	// No post-processing
	"subroutine(postProc) void none() {"
		"fragCol = texture(tex, tex_uv);"
	"}"

	// Linearize the depth buffer
	"subroutine(postProc) void linearizeDepth() {"
		"float z = texture(tex, tex_uv).r, n = 1.0, f = 100.0;"
		"z = (2.0 * n) / (f + n - z * (f - n));"
		"fragCol = vec4(z, z, z, 1.);"
	"}"

	"void main() {"
		"post_proc();"
	"}";

void Shaders::loadDefaultShaders() {
	if (!defLinked) {
		GLuint vertShader;
		GLuint fragShader;
		GLuint compShader;

		// Depth PrePass
		depthPrepassProgId = glCreateProgram();
		vertShader = loadShader(depthPrepassProgId,   GL_VERTEX_SHADER, Shaders::DEPTH_VERT_SHADER_PRGM);GLERRCHK
		fragShader = loadShader(depthPrepassProgId, GL_FRAGMENT_SHADER, Shaders::DEPTH_FRAG_SHADER_PRGM);GLERRCHK
		linkProgram(depthPrepassProgId);GLERRCHK
		glDetachShader(depthPrepassProgId, vertShader);GLERRCHK
		glDetachShader(depthPrepassProgId, fragShader);GLERRCHK
		glDeleteShader(vertShader);
		glDeleteShader(fragShader);GLERRCHK

		// Light Culling
		lightCullProgId = glCreateProgram();
		compShader = loadShader(lightCullProgId, GL_COMPUTE_SHADER, Shaders::LIGHTCULL_COMP_SHADER_PRGM);GLERRCHK
		linkProgram(lightCullProgId);GLERRCHK
		glDetachShader(lightCullProgId, compShader);GLERRCHK
		glDeleteShader(compShader);GLERRCHK

		// Shading
		shadingProgId = glCreateProgram();
		vertShader = loadShader(shadingProgId,   GL_VERTEX_SHADER, Shaders::SHADING_VERT_SHADER_PRGM);GLERRCHK
		fragShader = loadShader(shadingProgId, GL_FRAGMENT_SHADER, Shaders::SHADING_FRAG_SHADER_PRGM);GLERRCHK
		linkProgram(shadingProgId);GLERRCHK
		glDetachShader(shadingProgId, vertShader);GLERRCHK
		glDetachShader(shadingProgId, fragShader);GLERRCHK
		glDeleteShader(vertShader);
		glDeleteShader(fragShader);GLERRCHK

		// Post processing
		postProcProgId = glCreateProgram();
		vertShader = loadShader(postProcProgId,   GL_VERTEX_SHADER, Shaders::POSTPROC_VERT_SHADER_PRGM);GLERRCHK
		fragShader = loadShader(postProcProgId, GL_FRAGMENT_SHADER, Shaders::POSTPROC_FRAG_SHADER_PRGM);GLERRCHK
		linkProgram(postProcProgId);GLERRCHK
		glDetachShader(postProcProgId, vertShader);GLERRCHK
		glDetachShader(postProcProgId, fragShader);GLERRCHK
		glDeleteShader(vertShader);
		glDeleteShader(fragShader);GLERRCHK

		defLinked = true;
	}
}

void Shaders::unloadDefaultShaders() {
	glDeleteProgram(shadingProgId);
	glDeleteProgram(lightCullProgId);
	glDeleteProgram(depthPrepassProgId);
}

// Compiles and links a Shader
GLuint Shaders::loadShader(GLuint program, GLenum shaderType, const GLchar *source) {
	const GLchar **shaderSource = &source;

	// Compiles
	GLuint shader = glCreateShader(shaderType);
	glShaderSource(shader, 1, shaderSource, 0);
	glCompileShader(shader);

	int compiled = 0;
	glGetShaderiv(shader, GL_COMPILE_STATUS, &compiled);
	if(compiled == GL_FALSE) {
		char *failStr = new char[250];
		glGetShaderInfoLog(shader, 250, NULL, failStr);
		glDeleteShader(shader);
		throw Failure(failStr);
	}

	// Links
	glAttachShader(program, shader);
	return shader;
}

void Shaders::linkProgram(GLuint program) {
	int linked = 0;
	glLinkProgram(program);
	glGetProgramiv(program, GL_LINK_STATUS, &linked);
	if (linked == GL_FALSE) {
		char *failStr = new char[250];
		glGetProgramInfoLog(program, 250, NULL, failStr);
		glDeleteProgram(program);
		throw Failure(failStr);
	}
}
