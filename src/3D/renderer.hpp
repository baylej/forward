/**
	Forward+ renderer.
	See: https://amd.app.box.com/s/yltuiyps4y83926hz3mm74rc3u0t02en
 **/
#pragma once
#ifndef RENDERER_H
#define RENDERER_H

#include <vector>

#include <allegro5/allegro.h>

#include "glw.hpp"
#include "lights.hpp"
#include "shaders.hpp"
#include "camera.hpp"
#include "geometry.hpp"

class Renderer
{
	Shaders shaders;

	/// Frustum data
	int win_w, win_h, win_tile_w, win_tile_h;
	float near, far, fov;

	/// GL names of the frame buffer object used in the depth pre-pass
	GLuint fbo_depth;
	GLuint depth_tex;

	/// Tiles and GL name of the light index buffer
	int tile_w, tile_h, max_lights_per_tiles, tile_count;
	GLuint ssbo_light_indices;

	/// GL names of uniform variables in the depth pre-pass
	GLuint d_proj_mat, d_view_mat, d_model_mat;

	/// GL names of uniform variables in the light culling pass
	GLuint lc_proj_mat, lc_view_mat, lc_camera_pos_3d, lc_screen_size, lc_tile_size, lc_max_lights_per_tile, lc_depth;

	/// GL names of uniform variables in the shading pass
	GLuint s_proj_mat, s_view_mat, s_model_mat;

	/// GL names of post-proc subroutines, texture
	GLuint pp_none, pp_linearize_depth, pp_texture;

public:
	Lights light;
	Camera camera;

	/// Scene's geometry
	std::vector<IndexedMesh> geometry;
	std::vector<glm::mat4> geom_model;

	/// Initialize the Forward+ renderer
	Renderer(int win_w, int win_h, float near, float far, float fov, int tile_w, int tile_h, int max_lights_per_tiles);
	~Renderer();

	/// Resize the viewport
	/// Use this function instead of camera.frustum (it also updates an SSBO)
	void resize(int w, int h);

	/// Loads the Projection matrix and the View matrix
	/// Calls `prePass()`, `lightCull()` and `shading()`
	void render();

	/// Depth pre-pass
	void prePass();
	/// Light culling
	void lightCull();
	/// Shading
	void shading();
	/// Post processing (optional)
	void postProc(GLuint tex_name, GLuint pp_func_name);

	/// For each IndexedMesh, loads its model mat and call draw()
	/// Used by `prePass()` and `shading()`
	void drawGeometry(GLuint model_m4_target);

	/// Shows light density per tile, blue=0, green=max_light/2., red=max_light
	ALLEGRO_BITMAP* tilesDensity();

	/// Exports the depth prepass texture to a float array (len = win_w*win_h)
	float* depthComp();
};

#endif // RENDERER_H
