#include "camera.hpp"

void Camera::setPosition(float x, float y, float z) {
	cam_pos.x = x;
	cam_pos.y = y;
	cam_pos.z = z;
}

void Camera::setPosition(glm::vec3 pos) {
	cam_pos = pos;
}

void Camera::incPosition(float x, float y, float z) {
	cam_pos.x += x;
	cam_pos.y += y;
	cam_pos.z += z;
}

void Camera::incPosition(glm::vec3 pos) {
	cam_pos += pos;
}

glm::vec3 Camera::getPosition() {
	return cam_pos;
}

void Camera::setOrientation(float alpha, float gamma, float teta) {
	cam_orient.x = alpha;
	cam_orient.y = gamma;
	cam_orient.z = teta;
}

void Camera::setOrientation(glm::vec3 orient) {
	cam_orient = orient;
}

void Camera::incOrientation(float alpha, float gamma, float teta) {
	cam_orient.x += alpha;
	cam_orient.y += gamma;
	cam_orient.z += teta;
}

void Camera::incOrientation(glm::vec3 orient) {
	cam_orient += orient;
}

glm::vec3 Camera::getOrientation() {
	return cam_orient;
}

void Camera::foenum() {
	glm::mat4 cam_rotX = glm::rotate(    m4id, -cam_orient.x, Xu);
	glm::mat4 cam_rotY = glm::rotate(cam_rotX, -cam_orient.y, Yu);
	// Ignoring Z

	view_mat = glm::translate(cam_rotY, cam_pos * glm::vec3(-1.));

	cam_rotY = glm::rotate(    m4id, cam_orient.y, Yu);
	cam_rotX = glm::rotate(cam_rotY, cam_orient.x, Xu);
	// Ignoring Z

	inv_view_mat = glm::translate(cam_rotX, cam_pos);
}

void Camera::frustum(unsigned int width, unsigned int height, float znear, float zfar, float fov) {
	if (height==0) height=1;
	if (width==0)  width=1;
	glViewport(0, 0, (GLint)width, (GLint)height);

	float ratio = width/float(height);

	proj_mat = glm::perspective(fov, ratio, znear, zfar);
}

glm::vec3 Camera::getForwardVector() {
	return glm::vec3(glm::normalize(inv_view_mat * glm::vec4(0., 0., -1., 0.)));
}

glm::vec3 Camera::getRightVector() {
	return glm::vec3(glm::normalize(inv_view_mat * glm::vec4(1., 0., 0., 0.)));
}

glm::vec3 Camera::getUpVector() {
	return glm::vec3(glm::normalize(inv_view_mat * glm::vec4(0., 1., 0., 0.)));
}

void Camera::loadProjectionMatrix(GLuint proj_m4_target) {
	glUniformMatrix4fv(proj_m4_target, 1, GL_FALSE, glm::value_ptr(proj_mat));GLERRCHK
}

void Camera::loadViewMatrix(GLuint view_m4_target) {
	glUniformMatrix4fv(view_m4_target, 1, GL_FALSE, glm::value_ptr(view_mat));
}
