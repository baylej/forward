/**
	OpenGL Camera.
**/
#pragma once
#ifndef CAMERA_HPP
#define CAMERA_HPP

#include "glw.hpp"

/// `frustum` and `foenum` will convert camera transformations to world transformation.
/// Thus to move a camera to position [5, 5, 5], call `cam.setPosition(5, 5, 5)`;
class Camera {
	glm::vec3 cam_orient; // (alpha, gamma, teta) angles in rads
	glm::vec3 cam_pos;    // (X, Y, Z) position
public:
	glm::mat4 proj_mat, view_mat, inv_view_mat;

	void setPosition(float x, float y, float z);
	void setPosition(glm::vec3 pos);
	void incPosition(float x, float y, float z);
	void incPosition(glm::vec3 pos);
	glm::vec3 getPosition();

	void setOrientation(float alpha, float gamma, float teta);
	void setOrientation(glm::vec3 orient);
	void incOrientation(float alpha, float gamma, float teta);
	void incOrientation(glm::vec3 orient);
	glm::vec3 getOrientation();

	// Computes and store the view matrix for the current values in cam_pos and cam_orient
	void foenum();

	// Computes and store the projection matrix for the given parameters
	void frustum(unsigned int width, unsigned int height, float near, float far, float fov);

	// Calculates and returns the Forward, Right and Up vectors from the current cam_orient value
	glm::vec3 getForwardVector();
	glm::vec3 getRightVector();
	glm::vec3 getUpVector();

	// Loads the projection matrix in the "proj_m4" uniform mat4 (programmable rendering pipeline)
	void loadProjectionMatrix(GLuint shaderId);

	// Loads the view matrix in the "view_m4" uniform mat4 (programmable rendering pipeline)
	void loadViewMatrix(GLuint shaderId);
};

#endif // CAMERA_HPP
