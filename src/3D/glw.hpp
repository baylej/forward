/**
	GL Wrapper header.
**/

// Allegro load properly the gl header on every plateform.
//#include <allegro5/allegro.h>
//#include <allegro5/allegro_opengl.h>

// Because GLEW does not work with core contexts, using GLLoadGen(https://bitbucket.org/alfonse/glloadgen) instead.
// Included gl_core_43 header was generated by GLLoadGen v 2.0.3a with the command:
// lua LoadGen.lua -style=pointer_c -spec=gl -version=4.3 -profile=core core_43
#include "gl_core_43.h"

// GLM
#define GLM_FORCE_RADIANS
#include <glm/fwd.hpp>
#include <glm/glm.hpp>
#include <glm/mat4x4.hpp>
#include <glm/vec4.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

// X, Y, Z unit vectors
#define Xu glm::vec3(1., 0., 0.)
#define Yu glm::vec3(0., 1., 0.)
#define Zu glm::vec3(0., 0., 1.)

// 4x4 identity matrix
#define m4id glm::mat4(1.)

#include <iostream>
#include <cstdlib>

// Prints the GL error code, the file and the line.
#define GLERRCHK { int err = glGetError(); \
                   if(err != GL_NO_ERROR) { \
                       std::cerr << "Gl Error : " << err << " (" << __FILE__ << ':' << __LINE__ << ')' << std::endl; \
                       std::exit(1); \
                   } }

// 'near' and 'far' are defined by some windows headers
#undef near
#undef far
