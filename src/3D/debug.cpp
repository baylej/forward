#include "debug.hpp"

#include <iostream>

using std::cout;
using std::endl;

// Log callback
static void APIENTRY log_cb(GLenum source, GLenum type, GLuint id, GLenum severity, GLsizei length, const GLchar* message, const void* userParam) {
	OpenGLLogger *instance = (OpenGLLogger*)userParam;
	instance->log(source, type, id, severity, length, message);
}

void OpenGLLogger::registerOpenGLLogger() {
	glDebugMessageCallback(log_cb, this);
}

const char* OpenGLLogger::getSource(GLenum source) {
	const char *res;
	switch(source) {
		case GL_DEBUG_SOURCE_API:
			res = "OpenGL API";
			break;
		case GL_DEBUG_SOURCE_WINDOW_SYSTEM:
			res = "Window-system API";
			break;
		case GL_DEBUG_SOURCE_SHADER_COMPILER:
			res = "GLSL compiler";
			break;
		case GL_DEBUG_SOURCE_THIRD_PARTY:
			res = "Third party";
			break;
		case GL_DEBUG_SOURCE_APPLICATION:
			res = "User";
			break;
		case GL_DEBUG_SOURCE_OTHER:
			res = "Other";
			break;
		default:
			res = "Unknown";
	}
	return res;
}

const char* OpenGLLogger::getType(GLenum type) {
	const char *res;
	switch(type) {
		case GL_DEBUG_TYPE_ERROR:
			res = "Error";
			break;
		case GL_DEBUG_TYPE_DEPRECATED_BEHAVIOR:
			res = "Deprecated functionnality";
			break;
		case GL_DEBUG_TYPE_UNDEFINED_BEHAVIOR:
			res = "Undefined behavior";
			break;
		case GL_DEBUG_TYPE_PORTABILITY:
			res = "Functionnality not portable";
			break;
		case GL_DEBUG_TYPE_PERFORMANCE:
			res = "Performance issues";
			break;
		case GL_DEBUG_TYPE_MARKER:
			res = "Command stream annotation";
			break;
		case GL_DEBUG_TYPE_PUSH_GROUP:
			res = "Group pushing";
			break;
		case GL_DEBUG_TYPE_POP_GROUP:
			res = "Group poping";
			break;
		case GL_DEBUG_TYPE_OTHER:
			res = "Other";
			break;
		default:
			res = "Unknown";
	}
	return res;
}

const char* OpenGLLogger::getSeverity(GLenum severity) {
	const char *res;
	switch (severity) {
		case GL_DEBUG_SEVERITY_HIGH:
			res = "Error";
			break;
		case GL_DEBUG_SEVERITY_MEDIUM:
			res = "Warn";
			break;
		case GL_DEBUG_SEVERITY_LOW:
			res = "Debug";
			break;
		case GL_DEBUG_SEVERITY_NOTIFICATION:
			res = "Info";
			break;
		default:
			res = "Unknown";
	}
	return res;
}

void OpenGLLogger::log(GLenum source, GLenum type, GLuint, GLenum severity, GLsizei, const GLchar* message) {
	cout << '[' << getSeverity(severity) << "] (" << getSource(source) << ") " << getType(type) << ": " << message << endl;
}
