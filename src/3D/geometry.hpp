/**
	Basic geometry.
**/
#pragma once
#ifndef GEO_H
#define GEO_H

#include "glw.hpp"

/// AxisDrawer : draws the 3 axis X, Y and Z
class AxisDrawer {
	GLuint verticesVAO;
public:
	/* len = length of the lines */
	AxisDrawer(float len);
	void draw();
};

// ----

/// Indexed Mesh (GL_TRIANGLES)
class IndexedMesh {
	GLuint indicesVBO;
	GLuint verticesVAO;

	GLuint primCount;

public:

	/* `ind` and `vtx` and `col` must not be NULL
	   `ind` has `primCount` elements
	   `vtx`, `col`, `nrm`, `tuv` must have `pointCount` elements
	   each parameter may be freed. */
	IndexedMesh(unsigned int primCount, unsigned int pointCount,
	    const unsigned int ind[][3],
	    const float vtx[][3], const float col[][3],
	    const float nrm[][3], const float tuv[][2]);

	/* binds and draw the VAO */
	void draw();
};

// ----

/// Cube centered on the origin (0,0); Width = 1
class Cube: public IndexedMesh {
	static const float vertices[24][3];
	static const float normals[24][3];
	static const float colors[24][3];
	static const float texUV[24][2];
	static const unsigned int indices[12][3];
public:
	Cube(): IndexedMesh(12, 24, indices, vertices, colors, normals, texUV) {};
};

// ----

/// Plane centered on the origin (0,0), Width = 2 (-1,-1 to 1,1)
class Plane: public IndexedMesh {
	static const float vertices[4][3];
	static const float colors[4][3];
	static const float texUV[4][2];
	static const unsigned int indices[2][3];
public:
	Plane(): IndexedMesh(2, 4, indices, vertices, colors, NULL, texUV) {};
};

#endif /* GEO_H */
