#include "glw.hpp"
#include "geometry.hpp"
#include "../failure.hpp"

AxisDrawer::AxisDrawer(float len) {
	float coord[18] = {0.0, 0.0, 0.0,  len, 0.0, 0.0,
	                   0.0, 0.0, 0.0,  0.0, len, 0.0,
	                   0.0, 0.0, 0.0,  0.0, 0.0, len};
	float color[18] = {1.0, 0.0, 0.0,  1.0, 0.0, 0.0,
	                   0.0, 1.0, 0.0,  0.0, 1.0, 0.0,
	                   0.0, 0.0, 1.0,  0.0, 0.0, 1.0};

	GLuint vertexInfos[2];
	glGenBuffers(2, vertexInfos);
	glGenVertexArrays(1, &verticesVAO);
	glBindVertexArray(verticesVAO);

	glEnableVertexAttribArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, vertexInfos[0]);
	glBufferData(GL_ARRAY_BUFFER, 3*6*sizeof(GLfloat), coord, GL_STATIC_DRAW);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, 0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glEnableVertexAttribArray(3);
	glBindBuffer(GL_ARRAY_BUFFER, vertexInfos[1]);
	glBufferData(GL_ARRAY_BUFFER, 3*6*sizeof(GLfloat), color, GL_STATIC_DRAW);
	glVertexAttribPointer(3, 3, GL_FLOAT, GL_FALSE, 0, 0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glBindVertexArray(0);
}

void AxisDrawer::draw() {
	glBindVertexArray(verticesVAO);
	glDrawArrays(GL_LINES, 0, 6);
	glBindVertexArray(0);
}

IndexedMesh::IndexedMesh(unsigned int primCount,
         unsigned int pointCount, const unsigned int ind[][3],
         const float vtx[][3], const float col[][3],
         const float nrm[][3], const float tuv[][2])
{
	if (ind == NULL || vtx == NULL || col == NULL || primCount == 0 || pointCount < 3)
		throw Failure("The parameters `vtx` and `col` must not be NULL and must contain at least one element.");

	this->primCount  = primCount;

	// Tricky part to deal with the 2 optional parameters
	const void* buffers[4] = {vtx, col, NULL, NULL};
	unsigned int vboLen[4] = {3, 3, 0, 0};

	GLuint attribs[4] = {0, 3, 0, 0};
	int vInfoLen = 2;
	if (nrm != NULL) {
		attribs[vInfoLen] = 1;
		buffers[vInfoLen] = nrm;
		vboLen [vInfoLen] = 3;
		vInfoLen++;
	}
	if (tuv != NULL) {
		attribs[vInfoLen] = 2;
		buffers[vInfoLen] = tuv;
		vboLen [vInfoLen] = 2;
		vInfoLen++;
	}
	int vboCount = vInfoLen + 1;

	GLuint vertexInfos[5]; // 0:=ind  1:=vtx  2:=col  3:=nrm  4:=tuv
	GLERRCHK
	glGenBuffers(vboCount, vertexInfos);GLERRCHK
	glGenVertexArrays(1, &verticesVAO);GLERRCHK
	glBindVertexArray(verticesVAO);GLERRCHK

	// Bind vertex data from the RAM to the VBOs + set VAOs attributes
	for (int i=0; i<vInfoLen; i++) {
		glEnableVertexAttribArray(attribs[i]);
		glBindBuffer(GL_ARRAY_BUFFER, vertexInfos[i+1]);
		glBufferData(GL_ARRAY_BUFFER, vboLen[i]*pointCount*sizeof(GLfloat), buffers[i], GL_STATIC_DRAW);
		glVertexAttribPointer(attribs[i], vboLen[i], GL_FLOAT, GL_FALSE, 0, 0);
		glBindBuffer(GL_ARRAY_BUFFER, 0);GLERRCHK
	}

	// Bind `ind` to its VBO
	indicesVBO = vertexInfos[0];
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indicesVBO);GLERRCHK
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, 3*primCount*sizeof(GLuint), ind, GL_STATIC_DRAW);GLERRCHK
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);GLERRCHK

	glBindVertexArray(0);GLERRCHK
}

void IndexedMesh::draw() {
		glBindVertexArray(verticesVAO);GLERRCHK

		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indicesVBO);GLERRCHK
		glDrawElements(GL_TRIANGLES, primCount*3, GL_UNSIGNED_INT, 0);GLERRCHK

		glBindVertexArray(0);GLERRCHK
}

/**
	Cube
**/
const float Cube::vertices[24][3] = {
	{-0.5, -0.5,  0.5},  { 0.5, -0.5,  0.5},  { 0.5,  0.5,  0.5},  {-0.5,  0.5,  0.5},

	{-0.5, -0.5, -0.5},  {-0.5,  0.5, -0.5},  { 0.5,  0.5, -0.5},  { 0.5, -0.5, -0.5},

	{-0.5, -0.5, -0.5},  {-0.5, -0.5,  0.5},  {-0.5,  0.5,  0.5},  {-0.5,  0.5, -0.5},

	{ 0.5, -0.5,  0.5},  { 0.5, -0.5, -0.5},  { 0.5,  0.5, -0.5},  { 0.5,  0.5,  0.5},

	{-0.5,  0.5,  0.5},  { 0.5,  0.5,  0.5},  { 0.5,  0.5, -0.5},  {-0.5,  0.5, -0.5},

	{-0.5, -0.5,  0.5},  {-0.5, -0.5, -0.5},  { 0.5, -0.5, -0.5},  { 0.5, -0.5,  0.5}
};

const float Cube::normals[24][3] = {
	{ 0.,  0.,  1.},  { 0.,  0.,  1.},  { 0.,  0.,  1.},  { 0.,  0.,  1.},

	{ 0.,  0., -1.},  { 0.,  0., -1.},  { 0.,  0., -1.},  { 0.,  0., -1.},

	{-1.,  0.,  0.},  {-1.,  0.,  0.},  {-1.,  0.,  0.},  {-1.,  0.,  0.},

	{ 1.,  0.,  0.},  { 1.,  0.,  0.},  { 1.,  0.,  0.},  { 1.,  0.,  0.},

	{ 0.,  1.,  0.},  { 0.,  1.,  0.},  { 0.,  1.,  0.},  { 0.,  1.,  0.},

	{ 0., -1.,  0.},  { 0., -1.,  0.},  { 0., -1.,  0.},  { 0., -1.,  0.}
};

const float Cube::colors[24][3] = {
	{1., .9, 0.}, {1., .9, 0.}, {1., .9, 0.}, {1., .9, 0.},

	{.7, .8, .9}, {.7, .8, .9}, {.7, .8, .9}, {.7, .8, .9},

	{1., .7, .8}, {1., .7, .8}, {1., .7, .8}, {1., .7, .8},

	{.7, 1., .9}, {.7, 1., .9}, {.7, 1., .9}, {.7, 1., .9},

	{0., 1., .5}, {0., 1., .5}, {0., 1., .5}, {0., 1., .5},

	{.5, .8, .9}, {.5, .8, .9}, {.5, .8, .9}, {.5, .8, .9}
};

const float Cube::texUV[24][2] = {
	{0.0, 0.0},  {1.0, 0.0},  {1.0, 1.0},  {0.0, 1.0},

	{1.0, 0.0},  {0.0, 0.0},  {0.0, 1.0},  {1.0, 1.0},

	{1.0, 0.0},  {0.0, 0.0},  {0.0, 1.0},  {1.0, 1.0},

	{1.0, 0.0},  {0.0, 0.0},  {0.0, 1.0},  {1.0, 1.0},

	{0.0, 1.0},  {1.0, 1.0},  {1.0, 0.0},  {0.0, 0.0},

	{0.0, 0.0},  {1.0, 0.0},  {1.0, 1.0},  {0.0, 1.0}
};

const unsigned int Cube::indices[12][3] = {
	// Face front
	{ 0,  1,  2},  { 0,  2,  3},
	// Face back
	{ 4,  5,  6},  { 4,  6,  7},
	// Face left
	{ 8,  9, 10},  { 8, 10, 11},
	// Face right
	{12, 13, 14},  {12, 14, 15},
	// Face top
	{16, 17, 18},  {16, 18, 19},
	// Face down
	{20, 21, 22},  {20, 22, 23}
};

/**
	Plane
**/
const float Plane::vertices[4][3] = {
	{-1., -1.,  0.},  { 1., -1.,  0.},  { 1.,  1.,  0.},  {-1.,  1.,  0.}
};

const float Plane::texUV[4][2] = {
	{0.0, 0.0},  {1.0, 0.0},  {1.0, 1.0},  {0.0, 1.0}
};

const float Plane::colors[4][3] = {
	{1., .9, 0.}, {.7, .8, .9}, {.5, .8, .9}, {0., 1., .5}
};

const unsigned int Plane::indices[2][3] = {
	{ 0,  1,  2},  { 0,  2,  3},
};
