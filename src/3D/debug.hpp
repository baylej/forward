/**
	OpenGL 4.3+ Debug message log printer
	https://www.opengl.org/wiki/Debug_Output
**/
#pragma once
#ifndef DEBUG_HPP
#define DEBUG_HPP

#include "glw.hpp"

/// An OpenGL debug output logger. You have to call glEnable(GL_DEBUG_OUTPUT).
class OpenGLLogger {
public:
	/// Registers this logger against the current OpenGL context.
	void registerOpenGLLogger();

	/// GLenum to string
	const char* getSource(GLenum source);
	const char* getType(GLenum type);
	const char* getSeverity(GLenum severity);

	/// Default: logs to stdout.
	virtual void log(GLenum source, GLenum type, GLuint id, GLenum severity, GLsizei length, const GLchar* message);
};

#endif	/* DEBUG_HPP */

